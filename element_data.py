"""
documentation for element_data.py
is really found on its wiki page 
Data from CSV Files - Part 3 - SageMath for Undergraduates
https://sagemathnotes.miraheze.org/wiki/Data_from_CSV_Files_-_Part_3#Full_Fledged_Module_and_Test

"""



import csv    # import module for csv tasks

ix_atomic_number      = 0  # integer
ix_element            = 1  # string
ix_symbol             = 2  # string
ix_atomic_mass        = 3  # float
ix_number_of_neutrons = 4  # integer
ix_number_of_protons  = 5  # int
ix_period             = 7  # int
ix_group              = 8  # int
ix_phase              = 9  # string
ix_radioactive        = 10  #  boolean   in file yes or blank
ix_natural            = 11  #  boolean   in file yes or blank
ix_metal              = 12  #  boolean   in file yes or blank

element_data_dict     = {}




def to_group( a_value ):
    """
    Given a_value that is a string representing a group ( periodic table )
    return the corresponding number an int. 
    If a_value is blank, an empty string "" then
    return 0

    Other cases for a_value will cause the function to throw an error.
 
    """
    if a_value == "":
        return 0
    else:
        return int( a_value )
        
        
def begin():

    """
    Purpose: get the module ready to use
    returns: None
    """

    global element_data_dict


    file_name    = "periodic_table.csv"
    with open( file_name, 'r') as a_file:
        csv_reader        = csv.reader( a_file )    # make a csv_reader which can read the file
        element_line_list = list( csv_reader )      # convert the csv version to a list


    # for ix, i_element in enumerate( element_line_list ):            # get index with line items
    #     print( f">>element_list what do we have?  {i_element} {type( i_element )}") # print the item we hope is a float
    #     if ix >= 10:   # limit the output
    #         break


    for ix, i_line_data in enumerate( element_line_list ):
        if ix == 0:
            continue                   # this is the header line no data

        i_line_data[ix_atomic_number]        = int( i_line_data[ix_atomic_number] )
        i_line_data[ix_atomic_mass]          = float( i_line_data[ix_atomic_mass] )
        i_line_data[ix_number_of_neutrons]   = int( i_line_data[ix_number_of_neutrons] )
        i_line_data[ix_number_of_protons]    = int( i_line_data[ix_number_of_protons] )
        #i_line_data[ix_number_of_protons]    = int( i_line_data[ix_number_of_protons] )

        i_line_data[ix_period]               = int(      i_line_data[ix_period] )
        i_line_data[ix_group]                = to_group( i_line_data[ix_group] )      # some listed as blank, use my own convert
        #i_line_data[ix_phase]    = int( i_line_data[ix_phase] )

        i_line_data[ix_radioactive]          = bool( i_line_data[ix_radioactive] )
        i_line_data[ix_natural]              = bool( i_line_data[ix_natural] )

        i_line_data[ix_metal]                = bool( i_line_data[ix_metal] )


    # this will be indexed by element symbol
    element_data_dict     = {}

    for ix, i_line_data in enumerate( element_line_list ):
        if ix == 0:
            continue
        element_data_dict[ i_line_data[ix_symbol] ] =  i_line_data        
        
def get_data_by_symbol_index( atomic_symbol, ix_data ):

    """
    Purpose:
    get data for an atomic element:
        atomic_symbol  = symbol_for_element    ex: "C"
        ix_data        = index for data        ex: element_data.ix_atomic_mass

    returns: data for the element

    """
    global element_data_dict

    data_line         = element_data_dict[atomic_symbol]
    requested_data    = data_line[ ix_data ]

    # could have been done in one more confusing? line
    requested_data    = element_data_dict[atomic_symbol][ ix_data ]

    return requested_data

   
   
   
   
   
   
   