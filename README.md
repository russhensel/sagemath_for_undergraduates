Files for the wiki SageMath for Undergraduates
https://sagemathnotes.miraheze.org/wiki/Main_Page  


Status:
    Early Draft


Guide to files:

    Files have the same name as the wiki page but : changed to -
    Most are Jupyter Notebook files for SageMath
    Some are supporting .txt file, or .py files

    More comming.

    There are also some junk, scratch, or other files that really do not belong here
    best use the files in conjunction with the wiki.


    The directory wiki_dump has a compressed version of the whole wiki, probably only useful if you have a wiki to restor it to.
    It was created from media wiki, the main page is SageMath for Undergraduates  https://sagemathnotes.miraheze.org/wiki/Main_Page
