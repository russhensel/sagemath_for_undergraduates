# -*- coding: utf-8 -*-

import  sys
import  os
# ------------- local imports
print( f"sys.path = >{sys.path}<" )

sys.path.append( "D:/Russ/0000/python00/python3/_projects/backup" )
#                 D:\Russ\0000\python00\python3\_projects\backup

import directory_backup
from   app_global import AppGlobal

#----------------------

app = directory_backup.App(  )

# msg = os.getcwd()      # Return the current working directory
# print( f"current directory  os.getcwd() {msg}")

# here    = os.getcwd()
# AppGlobal.app_state.initial_dir  = here
# AppGlobal.app_

app.set_backup_name( r"Copy SageMath for GitLab"   )

# ------------ next line is for stuff backup from here to I:...._data
#app.parameters.set_source_here( "data_K"  )    # one of data source source_2 ..... see directory_backup.py -- move to parameters

app.set_source( a_dir = "D:/Russ/0000/python00/sagemath/wiki" ,           a_id = "ignore" )
app.set_dest(   a_dir = "D:/for_gitlab/sagemath_for_undergraduates",      a_id = "ignore" )

app.set_check_source_dest_match( False )  # False = tails need not match

app.set_logs(   r"D:\logs_rsh\2019Detail.log",          r"D:\logs_rsh\2019Summary.log",      )   # detail, summary

app.set_simulate_mode(     False )   # True to simulate, False really copy

app.app_state.max_dir_depth = 0   # -1 for unlimited, we start at 0 not 1

filter_object  = directory_backup.FFAll( )     # need to default this and test itfilter_object  = directory_backup.FFAllFiles( )
#filter_object  = directory_backup.FFMp4( )
#filter_object  = directory_backup.FFPythonSource()
#filter_object  = directory_backup.FFExtList( ["bat", "txt"]  )

#print( f"filter object =>{filter_object}" )
#app.add_filter_object(   filter_object )
app.app_state.log_skipped_flag = False     # True/False


app.run_gui()

app.clean_up()   # cant this be moved inside

#============================= other sources and dest =========
#app.set_source( r"D:\Russ\0000\SpyderP",              "MainPart on 2TorNot2T"         )

#app.set_dest(   r"I:\Test\DeerAnti",                 "Buffalo 3T Disk"               )
#app.set_dest(   r"d:\Temp\Test\Arduino",             "MainPart on 2TorNot2T"         )

#app.set_source( r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestSource",             "TheProfDee"         )
#app.set_dest(   r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestDest\TestSource",    "TheProfDee"     )